(function($) {
		
	$('.init-accordion_js').on('click', function() {

		var accordion__layout = $(this).parents('.accordion__layout'),
		accordion__content = $(this).siblings('.accordion__content_js');

		if (accordion__layout.hasClass('accordion__layout_active')) {
			accordion__content.slideUp();
			accordion__layout.removeClass('accordion__layout_active');

		} else {
			accordion__content.slideDown();
			accordion__layout.addClass('accordion__layout_active').siblings('.accordion__layout_active').removeClass('accordion__layout_active').find('.accordion__content_js').slideUp();
		}	
	}); 

})(window.jQuery);


