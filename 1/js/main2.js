(function($) {

	$('.tabs_js').on('click', function(event) {
		
		event.preventDefault();
		$('.content__item').hide();
		$($(this).attr('href')).fadeIn(); 

		$(this).addClass('tabs_active-js').siblings('.tabs_js').removeClass('tabs_active-js');
	}); 

})(window.jQuery);


