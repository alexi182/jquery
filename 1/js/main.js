(function($) {

	$('.menu__link_js').on('click', function(event) {
		
		var submenu = $(this).siblings('.sub-menu');
		
		event.preventDefault();
		
		if ($(this).hasClass('menu__link_active-js')) {
			
			submenu.slideUp();
			submenu.siblings('.menu__link').removeClass('menu__link_active-js');
			
			} else {
				submenu.slideDown();
				$(this).addClass('menu__link_active-js').parent().siblings('.menu__item').find('.menu__link').removeClass('menu__link_active-js');
		}
	});

})(window.jQuery);


