(function($) {

	$(window).on('scroll', function() {

		var scroll_top = $(document).scrollTop();
		
		$('.menu__link_js').each(function() {
			
			var block = $('.menu__link_js').attr('href');
			var coordinate = $(block).offset().top;
			var height = $(block).outerHeight();
		
			if (scroll_top <= coordinate && coordinate + height > scroll_top) {
				$('.menu__link_active-js').removeClass('menu__link_active-js');
				$(this).addClass('menu__link_active-js');
			} else {
				$(this).removeClass('menu__link_active-js');
			}
			console.log(coordinate);
		});		

	});

})(window.jQuery);
